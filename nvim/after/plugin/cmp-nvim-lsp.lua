require'cmp'.setup {
  sources = {
    { name = 'nvim_lsp' }
  }
}

-- The nvim-cmp almost supports LSP's capabilities so You should advertise it to LSP servers..
local capabilities = require('cmp_nvim_lsp').default_capabilities()
-- Setup language servers.
local lspconfig = require('lspconfig')

-- The following example advertise capabilities to `clangd`.
lspconfig.clangd.setup {
  capabilities = capabilities,
}
lspconfig.pyright.setup {
  capabilities = capabilities,
}
lspconfig.gopls.setup {
  capabilities = capabilities,
}
lspconfig.rust_analyzer.setup {
  -- Server-specific settings. See `:help lspconfig-setup`
  settings = {
    ['rust-analyzer'] = {},
  },
  capabilities = capabilities
}
lspconfig.lua_ls.setup {
    capabilities = capabilities
}
lspconfig.elixirls.setup {
    cmd = { vim.fn.expand("$HOME/.local/share/elixir-ls/language_server.sh") },
    capabilities = capabilities
}
