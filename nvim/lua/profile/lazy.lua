local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
vim.opt.rtp:prepend(lazypath)

return require('lazy').setup({

	{'junegunn/fzf', build = "fzf#install()"},
	'junegunn/fzf.vim',
	'vim-airline/vim-airline',
	{'nvim-treesitter/nvim-treesitter', build = ':TSUpdate'},
	'nvim-lua/plenary.nvim',
	'theprimeagen/harpoon',
	'tpope/vim-fugitive',
	'neovim/nvim-lspconfig',
	'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-buffer',
    'hrsh7th/cmp-path',
    'hrsh7th/cmp-cmdline',
	'hrsh7th/nvim-cmp',
	'hrsh7th/cmp-vsnip',
	'hrsh7th/vim-vsnip',
})
