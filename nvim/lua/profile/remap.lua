vim.g.mapleader = " "
vim.keymap.set("n", "<Leader>te", vim.cmd.Texplore)
vim.keymap.set("n", "<Leader>tt", vim.cmd.tabe)
vim.keymap.set("n", "<Leader>ex", vim.cmd.Ex)
vim.keymap.set("n", "<Leader>ff", vim.cmd.Files)
vim.keymap.set("n", "<Leader>ti", vim.lsp.buf.hover)
