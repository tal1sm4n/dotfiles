#!/bin/sh

sudo pacman -S -y tmux git xclip tig zsh neovim

chsh -s $(which zsh)
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

ln -sf ~/.dotfiles/.vimrc ~/.vimrc
ln -sf ~/.dotfiles/.tmux.conf ~/.tmux.conf
ln -sf ~/.dotfiles/.xinitrc ~/.xinitrc
ln -sf ~/.dotfiles/.Xresources ~/.Xresources
ln -sf ~/.dotfiles/.aliases ~/.aliases
ln -sf ~/.dotfiles/.zshrc ~/.zshrc
mkdir -p ~/.config
ln -s ~/.dotfiles/nvim ~/.config/nvim
ln -s ~/.dotfiles/i3 ~/.config/i3
mkdir -p ~/.local/bin
ln -s ~/.dotfiles/bin/tmux-sessioniser ~/.local/bin/tmux-sessioniser
echo "source ~/.aliases" >> ~/.bashrc
ln -sf ~/.dotfiles/custom.zsh-theme ~/.oh-my-zsh/themes/custom.zsh-theme

sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
nvim +PlugInstall +qall

git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
git config --global core.editor nvim
